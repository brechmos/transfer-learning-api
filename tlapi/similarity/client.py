import logging

from PIL import Image
from io import BytesIO
import requests
from imageio import imread
from tlapi.config import api_server
from configparser import ConfigParser

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('similarity')
log.setLevel(logging.DEBUG)

config = ConfigParser()
config.read('config.ini')

api_server = 'http://' + config['processor']['machine']

def get(pk=None):
    """
    Programmatic access into the end point
    """

    # Get the location info
    if pk is None:
        response = requests.get('{}/similarity'.format(api_server))
    else:
        response = requests.get('{}/similarity/{}'.format(api_server, pk))

    return response.json()


def calculate(fingerprint_pks, similarity_calculator):
    """
    Programmatic access into the end point
    """

    log.debug('Going to call calculate with {} {}'.format(
        fingerprint_pks, similarity_calculator))
    response = requests.post('{}/similarity/calculate'.format(api_server),
            json={'fingerprint_pks': fingerprint_pks,
                  'similarity_calculator': similarity_calculator
                  })

    return response.json()


