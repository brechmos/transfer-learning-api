from tlapi.data import api as data_api
from tlapi.data import client as data_client
from tlapi.fingerprint import client as fingerprint_client
from tlapi.similarity import client as similarity_client
import shutil
import threading
import time
import os.path
import pickle
import pymongo
from tlapi.database import UnQLite
import shutil

def stringify(dictionary):
    return {k: str(v) for k, v in dictionary.items()}

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

from transfer_learning.fingerprint import FingerprintResnet
fresnet = FingerprintResnet()

db_key = UnQLite.key
print('key is {}'.format(db_key))

# Load the data
filename_prepend = 'http://18.218.192.161:4123/ACSimages/'
processing_dict = pickle.load(open('../../../transferlearning/examples/data/hubble_acs.pck', 'rb'))

N = 500

for fileinfo in processing_dict[:N]:
    im = {
        'location': os.path.join(filename_prepend, os.path.basename(fileinfo['location'])),
        'radec': fileinfo['radec'],
        'meta': stringify(fileinfo['meta'])
        }
    pk = data_client.save(im)
    print('   save pk is {}'.format(pk))
print('data {}'.format([x for x in data_client.get()]))
print('data pks {}'.format([str(x[db_key]) for x in data_client.get()]))

threaded = True
if threaded == True:
    tt = [x[db_key] for x in data_client.get()]
    for data in chunks(tt, len(tt)//N):
        t = threading.Thread(target=fingerprint_client.calculate, args=(data, fresnet.save()))
        t.daemon = True
        t.start()
else:
    for data in [x[db_key] for x in data_client.get()]:
        fingerprint_client.calculate(data, fresnet.save())

fingerprints = fingerprint_client.get() 
print('fingerprint pks {}'.format([str(x[db_key]) for x in fingerprints]))

 
print('Going to calculate the similarity')
simres = similarity_client.calculate([str(x[db_key]) for x in fingerprints], 'tsne')

print('Going to calculate the similarity jaccard')
simres = similarity_client.calculate([str(x[db_key]) for x in fingerprints], 'jaccard')

print('Waiting 3 seconds...')
time.sleep(3)

# Get all similarities
sims = similarity_client.get()
print('All similarities {}'.format(len(sims)))
