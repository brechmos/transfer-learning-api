from tlapi.data import api as data_api
from tlapi.data import client as data_client
from tlapi.fingerprint import client as fingerprint_client
from tlapi.similarity import client as similarity_client
import shutil
import time
import os.path
import pickle
import pymongo
from tlapi.database import Mongo, BlitzDB
import shutil

def stringify(dictionary):
    return {k: str(v) for k, v in dictionary.items()}

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

from transfer_learning.fingerprint import FingerprintResnet
fresnet = FingerprintResnet()

db = BlitzDB('/tmp/deleteme')

# Load the data
filename_prepend = 'http://18.218.192.161:4123/ACSimages/'
processing_dict = pickle.load(open('/Users/crjones/Documents/Science/TL/transfer_learning/examples/hubble_acs.pck', 'rb'))

N = 5

for fileinfo in processing_dict[:N]:
    im = {
        'location': os.path.join(filename_prepend, os.path.basename(fileinfo['filename'])),
        'radec': fileinfo['radec'],
        'meta': stringify(fileinfo['meta'])
        }
    data_client.save(im)

for pk in [x[db.key] for x in data_client.get()]:
    fingerprint_client.calculate(pk, fresnet.save())

fingerprints = fingerprint_client.get() 
print('fingerprint pks {}'.format([str(x[db.key]) for x in fingerprints]))

 
#print('Going to calculate the similarity')
#simres = similarity_client.calculate([str(x[db.key]) for x in fingerprints], 'tsne')
#
#time.sleep(2)
#
#print('Going to calculate the similarity jaccard')
#simres = similarity_client.calculate([str(x[db.key]) for x in fingerprints], 'jaccard')
#
#print('Waiting 3 seconds...')
#time.sleep(3)
#
## Get all similarities
#sims = similarity_client.get()
#print('All similarities {} {}'.format(len(sims), sims))
