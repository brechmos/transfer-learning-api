from tlapi.data import api as data_api
from tlapi.data import client as data_client
from tlapi.fingerprint import client as fingerprint_client
from tlapi.similarity import client as similarity_client
import shutil
import time
import os.path
import pickle
import pymongo
from tlapi.database import Mongo, BlitzDB
import shutil

import sys
sys.path.append('/Users/crjones/PycharmProjects/TransferLearning')

def stringify(dictionary):
    return {k: str(v) for k, v in dictionary.items()}

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

from transfer_learning.fingerprint import FingerprintResnet
fresnet = FingerprintResnet()

# Setup the database connection
db = Unqlite()

# Load the data
filename_prepend = 'http://18.218.192.161:4123/ACSimages/'
processing_dict = pickle.load(open('hubble_acs.pck', 'rb'))

N = 20

for fileinfo in processing_dict[:N]:
    im = {
        'location': os.path.join(filename_prepend, os.path.basename(fileinfo['filename'])),
        'radec': fileinfo['radec'],
        'meta': stringify(fileinfo['meta'])
        }
    data_client.save(im)

tt = [x[db.key] for x in db.find('data')]
for data in chunks(tt, N//2):
    fingerprint_client.calculate(data, fresnet.save())

fingerprints = fingerprint_client.get() 
print('fingerprint pks {}'.format([str(x[db.key]) for x in fingerprints]))

# 
print('Going to calculate the similarity')
simres = similarity_client.calculate([str(x[db.key]) for x in fingerprints], 'tsne')

time.sleep(2)

print('Going to calculate the similarity jaccard')
simres = similarity_client.calculate([str(x[db.key]) for x in fingerprints], 'jaccard')

print('Waiting 10 seconds...')
time.sleep(10)

# Get all similarities
sims = similarity_client.get()
print('All similarities {} {}'.format(len(sims), sims))
