from flask import Flask, request, Blueprint, jsonify, current_app
import sys
import numpy as np

from tlapi.data.api import get as get_data
from tlapi.data.api import get_array as get_array_data
import json
import logging
from tlapi.utils import gzipped
from tlapi.fingerprint import processing as fingerprint_processing

from transfer_learning.fingerprint import Fingerprint as FingerprintCalculator

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('fingerprint')
log.setLevel(logging.DEBUG)

blueprint = Blueprint('Fingerprints', __name__, url_prefix='/fingerprints')

@blueprint.route('/', methods=['GET', 'POST'])
@blueprint.route('/<pk>', methods=['GET'])
@gzipped
def get(pk=None):
    log.debug('fingerprint get with pk {}'.format(pk))

    data = fingerprint_processing.get(current_app.db, pk)

    return jsonify(data)

@blueprint.route('/calculate', methods=['POST'])
@gzipped
def calculate():
    log.debug('fingerprint calculate')

    tt = request.get_json()
    data_pk = tt['data_pk']
    fingerprint_calculator = tt['fingerprint_calculator']

    if 'fingerprint_calculator' in current_app.fingerprint_calculator and fingerprint_calculator['uuid'] in current_app.fingerprint_calculator['fingerprint_calculator']:
        fc = current_app.fingerprint_calculator[fingerprint_calculator['uuid']]
    else:
        fc = FingerprintCalculator.load_parameters(fingerprint_calculator)
        current_app.fingerprint_calculator = {}
        current_app.fingerprint_calculator[fingerprint_calculator['uuid']] = fc

    toreturn = fingerprint_processing.calculate(current_app.db, fc, data_pk)

    return jsonify(toreturn)

def calculate_one(data_pk, fc):
    log.debug('calculate_one:  data_pk is {}'.format(data_pk))

    resp = get_array_data(data_pk)
    data = json.loads(resp.data.decode('utf-8'))

    predictions = fc.calculate(np.array(data)[:224,:224])
    predictions = [(x[0], x[1], np.float64(x[2])) for x in predictions]

    fingerprint_pk = current_app.db.save('fingerprint', {'data_pk': data_pk, 'predictions': predictions})

    # Have to convert to float64 so that it is json serializable
    return {
        'fingerprint_pk': fingerprint_pk,
        'predictions': predictions
        }
