from flask import Flask, request, Blueprint
import os

from tlapi.data.api import blueprint as blueprint_data
from tlapi.fingerprint.api import blueprint as blueprint_fingerprint
from tlapi.similarity.api import blueprint as blueprint_similarity
import logging
from configparser import ConfigParser

from tlapi.database import Mongo, BlitzDB, UnQLite

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('run')
log.setLevel(logging.DEBUG) 

config = ConfigParser()
config.read('config.ini')

def create_app():
    log.info('create_app')
    app = Flask(__name__)
    app.secret_key = '23rkjhqwfluyh234krjhn'
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

    app.register_blueprint(blueprint_data)
    app.register_blueprint(blueprint_fingerprint)
    app.register_blueprint(blueprint_similarity)

    @app.before_first_request
    def create_db():
        log.info('Initializing the database')

        if config['database']['type'] == 'blitzdb':
            log.info('  blitzdb with filename {}'.format(config['database']['filename']))
            app.db = BlitzDB(config['database']['filename'])
        elif config['database']['type'] == 'unqlite':
            log.info('  unqlite with filename {}'.format(config['database']['filename']))
            app.db = UnQLite(config['database']['filename'])
        elif config['database']['type'] == 'mongo':
            log.info('  mongo at {}'.format(config['database']['machine']))
            app.db = Mongo(config['database']['machine'])
        else:
            raise Exception('Unknown database type, {}, in the configuration file'.format(config['database']['type']))
        app.fingerprint_calculator = {}

    return app
