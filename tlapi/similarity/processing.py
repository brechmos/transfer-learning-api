import sys
import numpy as np
import threading

from tlapi.fingerprint.api import get as get_fingerprint
from tlapi.fingerprint import client as fingerprint_client
import logging
import json
from tlapi.utils import gzipped

from transfer_learning.fingerprint import Fingerprint
from transfer_learning.similarity import tSNE, Jaccard, Distance

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('similarity')
log.setLevel(logging.DEBUG)

def get(db, pk=None):
    log.info('similarity get with pk {}'.format(pk))

    if pk is None:
        # Get all the data
        data = db.find(table='similarity')
    else:
        # Get the data based on the pk
        data = db.find(table='similarity', key=pk)
    return data

def calculate(db, fingerprint_pks, similarity_calculator):
    log.info('similarity calculate')

    # Save the dummy value to get the 
    similarity_id = db.save(table='similarity', data={'calculating': True})

    # Now start the actual processing in a different thread.
    if False:
        t = threading.Thread(target=real_calculate, name='real_calculate',
                args=(db, fingerprint_pks, similarity_calculator, similarity_id))
        t.daemon = True
        t.start()
    else:
        real_calculate(db, fingerprint_pks, similarity_calculator, similarity_id)

    similarity_result = {}
    # Have to convert to float64 so that it is json serializable
    return similarity_result


def real_calculate(db, fingerprint_pks, similarity_calculator, sim_res_pk):
    log.info('Start threaded real_calculate {} fingerprints and simcalc {}'.format(len(fingerprint_pks), similarity_calculator))

    if similarity_calculator == 'tsne':
        sim = tSNE()
    elif similarity_calculator == 'jaccard':
        sim = Jaccard()
    elif similarity_calculator == 'distance':
        sim = Distance()

    fingerprints = fingerprint_client.get(fingerprint_pks)

    sim.calculate(fingerprints)

    # Save the data into the database
    similarity_result = sim.get_similarity(pk=db.key)
    similarity_result['fingerprints'] = fingerprint_pks

    # Need to convert for json
    tt = db.update(table='similarity', key=sim_res_pk, data=similarity_result)

    log.debug('Done threaded real_calculate {}'.format(similarity_calculator))
