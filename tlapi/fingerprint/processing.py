import sys
import numpy as np

from tlapi.data.api import get as get_data
from tlapi.data.api import get_array as get_array_data
import json
import logging
from tlapi.utils import gzipped

from transfer_learning.fingerprint import Fingerprint as FingerprintCalculator

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('fingerprint')
log.setLevel(logging.DEBUG)

def get(db, pk=None):
    log.info('fingerprint get with pk {}'.format(pk))
    if pk is None:
        # Get all the data
        data = db.find(table='fingerprint')
    else:
        # Get the data based on the pk
        data = db.find('fingerprint', pk) 
    return data

def calculate(db, fc, data_pk):
    log.info('fingerprint calculate')

    if isinstance(data_pk, list):
        toreturn = []
        for dpk in data_pk:
            blah = calculate_one(db, dpk, fc)
            toreturn.append(blah)
        return toreturn
    else:
        return calculate_one(db, data_pk, fc)

def calculate_one(db, data_pk, fc):
    log.info('calculate_one:  data_pk is {}'.format(data_pk))

    resp = get_array_data(data_pk)
    data = json.loads(resp.data.decode('utf-8'))

    try:
        predictions = fc.calculate(np.array(data)[:224,:224])
        predictions = [(x[0], x[1], np.float64(x[2])) for x in predictions]

        fingerprint_pk = db.save('fingerprint', {'data_pk': data_pk, 'predictions': predictions})
    except ValueError as e:
        log.error('Error in calculating fingerprint ({})'.format(e))
        return {'fingerprint_pk': None, 'predictions': [] }


    # Have to convert to float64 so that it is json serializable
    return {
        'fingerprint_pk': fingerprint_pk,
        'predictions': predictions
        }
