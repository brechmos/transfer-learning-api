from flask import Flask, request, Blueprint, jsonify, current_app, abort
import sys
import numpy as np
import threading

from tlapi.fingerprint.api import get as get_fingerprint
from tlapi.fingerprint import client as fingerprint_client
import logging
import json
from tlapi.utils import gzipped
from tlapi.similarity import processing as similarity_processing

from transfer_learning.fingerprint import Fingerprint
from transfer_learning.similarity import tSNE, Jaccard, Distance

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('similarity')
log.setLevel(logging.DEBUG)

blueprint = Blueprint('Similarity', __name__, url_prefix='/similarity')


@blueprint.route('/', methods=['GET'])
@blueprint.route('/<pk>', methods=['GET'])
@gzipped
def get(pk=None):
    log.info('similarity get with pk {}'.format(pk))

    data = similarity_processing.get(current_app.db, pk)

    return json.dumps(data)

@blueprint.route('/calculate', methods=['POST'])
@gzipped
def calculate():
    log.info('similarity calculate')

    # Get the data out of the body
    tt = request.get_json()
    fingerprint_pks = tt['fingerprint_pks']
    similarity_calculator = tt['similarity_calculator']

    similarity_result = similarity_processing.calculate(current_app.db, fingerprint_pks, similarity_calculator)

    return jsonify(similarity_result)
