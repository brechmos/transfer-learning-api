import bson

def clean_integers(dictionary):
    return {(k, str(v) if isinstance(v, int) else v) for k, v in dictionary.items()}

def remove_objectid(dictionary):
    return {(k, str(v) if isinstance(v, bson.ObjectId) else v) for k, v in dictionary.items()}

def stringify(dictionary):
    return {k: str(v) for k, v in dictionary.items()}

def convert_objectid(dct):
    return {k: v if not isinstance(v, ObjectId) else str(v) for k,v in dct.items()}

