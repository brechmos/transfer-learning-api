from tlapi.data import api as data_api
from tlapi.data import client as data_client
from tlapi.fingerprint import client as fingerprint_client
from tlapi.similarity import client as similarity_client
import shutil
import time
import os.path
import pickle
import pymongo
from tlapi.database import Mongo, BlitzDB
import shutil

def stringify(dictionary):
    return {k: str(v) for k, v in dictionary.items()}

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

from transfer_learning.fingerprint import FingerprintResnet
fresnet = FingerprintResnet()

db = BlitzDB('/tmp/deleteme')

data = data_client.get() 
print('There were {} data'.format(len([x for x in data])))

fingerprints = fingerprint_client.get() 
print('There were {} fingerprints'.format(len([x for x in fingerprints])))

similiartys = similarity_client.get() 
print('There were {} similiartys'.format(len([x for x in similiartys])))
