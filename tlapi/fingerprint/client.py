import logging

from PIL import Image
from io import BytesIO
import requests
from imageio import imread
import json
from tlapi.config import api_server
from configparser import ConfigParser

import numpy as np

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-10s %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger('fingerprint')
log.setLevel(logging.DEBUG)

config = ConfigParser()
config.read('config.ini')

api_server = 'http://' + config['processor']['machine']

def get(pk=None):
    """
    Programmatic access into the end point
    """

    # Get the location info
    if pk is None:
        response = requests.get('{}/fingerprints'.format(api_server))
    elif isinstance(pk, list):
        response = requests.post('{}/fingerprints'.format(api_server), json={'pk': pk})
    else:
        response = requests.get('{}/fingerprints/{}'.format(api_server, pk))

    return response.json()


def calculate(pk, fingerprint):
    """
    Programmatic access into the end point
    """

    log.debug('Going to call calculate with {} {}'.format(pk, fingerprint))
    response = requests.post('{}/fingerprints/calculate'.format(api_server),
            json={'data_pk': pk, 'fingerprint_calculator': fingerprint})

    if not response.status_code == 200:
        log.debug('    response is {} {}'.format(response.status_code, response.text))
        return {}

    return response.json()
